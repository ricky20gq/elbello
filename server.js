var express = require('express'); //es la referencia a la dependencias
var userfile =require('./user.json');
var BodyParse = require('body-parser');
var app = express();
app.use(BodyParse.json());
const PORT = 3000;
const URL_BASE='/api-peru/v1/';
// operaciones GET del "hola mundo"

app.get(URL_BASE + 'users', function (request,response){
             response.status(200);
             response.send(userfile);
           });

  app.get(URL_BASE + 'users/:id/:id2', function (request,response){
                        let indice =request.params.id;
                        let id2 = request.params.id2;
                        console.log(indice);
                        console.log(id2);
                        response.status(200);
                        response.send(userfile[indice-1]);
                      });

  app.get(URL_BASE + 'usersq', function (request,response){
              let query =request.query;
              let auxiliar=[];
              for (var i =0;i<query.max;i++)
              {
                auxiliar.push(userfile[i]);
              }

            response.status(200);
            response.send(auxiliar);
                                          });
   app.get (URL_BASE + 'userstotalprofe',
           function(request,response){
           console.log(request.query);
           let total=userfile.length;
           let totalJson = JSON.stringify({num_elem : total});
           response.send(totalJson);
             response.status(200);
                         });

   //Nueva funcion de Post
   app.post(URL_BASE + 'users',
           function(req,res){

             let newUser = {
                 id: userfile.length+ 1,
                 first_name:req.body.first_name,
                 last_name:req.body.last_name,
                 email:req.body.email,
                 password:req.body.password
                            }
            userfile.push(newUser);
            res.status(201);
            res.send({ "mensaje":"usuario creado correctamente",
                       "usuario": newUser,
                       "userfile actualizado": userfile });

            });

            //operciòn POST a users : update
            app.put(URL_BASE + 'users/:id',
             function(req, res){
               console.log(req.body);
               console.log(req.body.id);
               let indice = req.params.id;
               let newUser = {
                 id: req.body.id,
                 first_name: req.body.first_name,
                 last_name: req.body.last_name,
                 email: req.body.email,
                 password: req.body.password
               }
               userFile[indice-1]= newUser;
               res.status(201);
               res.send({"mensaje":"Usuario actualizado correctamente",
                         "usuario":req.body.id,
                         "userFile actualizado":userFile});
            });
            //operciòn Delete ....
            app.delete(URL_BASE + 'users/:id',
             function(req, res){
               let indice = req.params.id;
               let respuesta = (userFile[indice-1] == undefined) ?
                 {"msg":"no existe"} :
                 {"Usuario eliminado" : (userFile.splice(indice-1, 1))};
              res.status(200).send(respuesta);
            });

app.listen(PORT,function(){
  console.log('node js esta escuchando los gritos de Javi......');
});
